<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                "name" => "permission-list"
            ],
            [
                "name" => "permission-create"
            ],
            [
                "name" => "permission-edit"
            ],
            [
                "name" => "permission-delete"
            ],
            [
                "name" => "role-list"
            ],
            [
                "name" => "role-create"
            ],
            [
                "name" => "role-edit"
            ],
            [
                "name" => "role-delete"
            ]
        ];

        foreach ($permissions as $key => $permission) {
            Permission::updateOrCreate(
                [
                    "id" => $key + 1
                ],
                $permission
            );
        }
    }
}
