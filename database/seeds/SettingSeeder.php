<?php

use Illuminate\Database\Seeder;

use App\Models\Setting;

class SettingSeeder extends Seeder
{
    public function run()
    {
        $settings = [
            [
                "id" => 'dashboard',
                "value" => 'This is a dashboard.'
            ],
            [
                "id" => 'history',
                "value" => 'This is a history.'
            ]
        ];

        foreach ($settings as $key => $setting) {
            Setting::updateOrCreate(
                [
                    "id" => $setting["id"]
                ],
                $setting
            );
        }
    }
}
