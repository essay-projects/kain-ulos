<?php

use Illuminate\Database\Seeder;

use App\Models\Usability;

class UsabilitySeeder extends Seeder
{
    public function run()
    {
        $usabilities = [
            [
                "name" => 'Kelahiran'
            ],
            [
                "name" => 'Kematian'
            ],
            [
                "name" => 'Pernikahan'
            ]
        ];

        foreach ($usabilities as $key => $usability) {
            Usability::updateOrCreate(
                [
                    "id" => $key + 1
                ],
                $usability
            );
        }
    }
}
