<?php

use Illuminate\Database\Seeder;

use App\Models\Person;

class PersonSeeder extends Seeder
{
    public function run()
    {
        $people = [
            [
                "full_name" => "sintasTech",
                "avatar" => "seeds/images/avatars/default.png"
            ],
            [
                "full_name" => "Admin",
                "avatar" => "seeds/images/avatars/default.png"
            ]
        ];

        foreach ($people as $key => $person) {
            Person::updateOrCreate(
                [
                    "id" => $key + 1
                ],
                $person
            );
        }
    }
}
