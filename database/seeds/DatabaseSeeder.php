<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            // Master Data
            PermissionSeeder::class,
            UsabilitySeeder::class,
            SettingSeeder::class,

            // User
            PersonSeeder::class,
            UserSeeder::class

            // Relation
        ]);
    }
}
