<header class="header_area clearfix">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-12 h-100">
                <div class="menu_area h-100">
                    <nav class="navbar h-100 navbar-expand-lg align-items-center">
                        <img src="{{ asset('assets/images/logo.png')}}" class="image-responsive" alt="logo" style="height: 55px"></a>
                        <!-- <h3 class="text-light m-0 ml-2">Kain Ulos Batak Toba</h3> -->
                        <span class="text-white m-0 ml-2 font-weight-bold" style="font-size: 9pt ; font-family: 'Times New Roman' ">
                            History
                            <br />KAIN ULOS
                            <br />Batak Toba
                        </span>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                        <div class="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                            <ul class="navbar-nav animated" id="nav">
                                <li class="nav-item {{ request()->routeIs('home.dashboards.*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home.dashboards.index') }}">Beranda</a></li>
                                <li class="nav-item {{ request()->routeIs('home.histories.*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home.histories.index') }}">Sejarah</a></li>
                                <li class="nav-item dropdown {{ request()->routeIs('home.usabilities.*') ? 'active' : '' }}">
                                    <a class="nav-link dropdown-toggle" href="#" id="moshDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Jenis</a>
                                    <div class="dropdown-menu" aria-labelledby="moshDropdown">
                                        @forelse ($varUsabilities as $usability)
                                        <a class="dropdown-item" href="{{ route('home.usabilities.show', $usability->id) }}">{{ $usability->name }}</a>
                                        @empty
                                        @endforelse
                                        <a class="dropdown-item" href="{{ route('home.usabilities.index') }}">Semua</a>
                                    </div>
                                </li>
                            </ul>

                            <div class="search-form-area animated">
                                <form action="{{ route('home.searches.index') }}" method="POST">
                                    @csrf
                                    <input type="search" name="keywords" id="search" placeholder="Apa nama kain ulos yang ingin Anda cari nih...">
                                    <button type="submit" class="d-none"><img src="{{ asset('themes/mosh/img/core-img/search-icon.png') }}" alt="Search"></button>
                                </form>
                            </div>

                            <div class="search-button">
                                <a href="#" id="search-btn" class="mr-4 ml-4"><img src="{{ asset('themes/mosh/img/core-img/search-icon.png') }}" alt="Search"></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>