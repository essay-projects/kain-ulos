<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Kain Ulos Batak Toba</title>
    <link rel="icon" href="{{ asset('assets/images/logo.png') }}">

    <!-- Styles -->
    <link href="{{ asset('themes/mosh/style.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/mosh/css/responsive.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body>
    <div id="preloader">
        <div class="mosh-preloader"></div>
    </div>

    @include('home.layouts.header')

    @yield('contents')

    @include('home.layouts.footer')

    <!-- JavaScript -->
    <script src="{{ asset('themes/mosh/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('themes/mosh/js/popper.min.js') }}"></script>
    <script src="{{ asset('themes/mosh/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/mosh/js/plugins.js') }}"></script>
    <script src="{{ asset('themes/mosh/js/active.js') }}"></script>
    @yield('javascript')
</body>

</html>