@extends('home.layouts.template')

@section('css')
<style>
    p {
        font-size: 18.72pt;
        color: black;
    }
</style>
@endsection

@section('contents')
<div class="mosh-breadcumb-area" style="background-image: url(<?= asset('themes/mosh/img/core-img/breadcumb.png') ?>);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="bradcumbContent">
                    <h2 class="text-left">{{ $item->name }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($item->images->count())
<section class="mosh-service-area clearfix pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="mosh-service-slides owl-carousel">
                    @foreach($item->images as $image)
                    <div class="single-service-area text-center">
                        <img src="{{ url($image->file) }}" class="image-responsive" alt="logo" style="height: 150px; width: 150px;"></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<section class="mosh-service-area clearfix pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="mosh-about-us-content">
                    <div class="section-heading pt-4 mb-2">
                        <h2>{{ $item->name }}</h2>
                    </div>
                    <p class="text-justify" style="size: 32px">{!! $item->description !!}</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection