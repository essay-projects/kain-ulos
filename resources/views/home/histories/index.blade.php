@extends('home.layouts.template')

@section('css')
<style>
    p {
        font-size: 18.72pt;
        color: black;
    }
</style>
@endsection

@section('contents')
<div class="mosh-breadcumb-area" style="background-image: url(<?= asset('themes/mosh/img/core-img/breadcumb.png') ?>);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="bradcumbContent">
                    <h2 class="text-left">Sejarah</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mosh-aboutUs-area section_padding_100_0">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="mosh-about-us-content">
                    <h2>Sejarah-Kejadian Ulos</h2>
                    <p class="text-black">{!! $history !!}</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection