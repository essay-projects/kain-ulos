@extends('home.layouts.template')

@section('css')
<style>
    #moreText {
        display: none;
    }
</style>
@endsection

@section('contents')
<div class="mosh-breadcumb-area" style="background-image: url(<?= url('themes/mosh/img/core-img/breadcumb.png') ?>)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="bradcumbContent">
                    <h2>{{ $usability->name }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mosh-aboutUs-area section_padding_100_0">
    @forelse ($usability->items as $item)
    <div class="container section_padding_50 pt-4 pb-4">
        <div class="row align-items-center">
            <div class="col-12 col-md-3">
                <div class="mosh-team-slides owl-carousel">
                    @forelse ($item->images as $image)
                    <div class="single-team-slide text-center">
                        <img src="{{ url($image->file) }}" style="width:100%; height: 200px" alt="" />
                    </div>
                    @empty
                    @endforelse
                </div>
            </div>

            <div class="col-12 col-md-9">
                <div class="mosh-about-us-content">
                    <div class="section-heading1 mb-3">
                        <h2 class="text-justify">{{ $item->name }}</h2>
                    </div>

                    <?php
                    $string = strip_tags($item->description);
                    if (strlen($string) > 250) {
                        $moreText = $string;
                        $stringCut = substr($string, 0, 250);
                        $endPoint = strrpos($stringCut, ' ');

                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);

                        $moreText = str_replace($string, "", $moreText);

                        $string .= '<span name="point[' . $item->id . ']">... </span><span name="more_text[' . $item->id . ']" hidden>' . $moreText . '</span> <button name="more_button[' . $item->id . ']" class="btn btn-sm btn-warning" onclick="handleMoreText(' . $item->id . ')">+ Selengkapnya</button>';
                    } else {
                    }
                    ?>
                    <p name="p[{{ $item->id }}]" class="h5 text-justify">
                        {!! $string !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    @empty
    <div class="container section_padding_50 pt-4 pb-4">
        <div class="row align-items-centers">
            <div class="col-12">
                <div class="mosh-about-us-content">
                    <div class="section-heading1 text-center mb-3">
                        <h4 class="font-italic text-muted">Wah, nampaknya tidak ada item pada jenis kain ulos {{ $usability->name }}.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforelse
</section>
@endsection


@section('javascript')
<script>
    const handleMoreText = (id) => {
        const moreButton = $('button[name="more_button[' + id + ']"]');
        if (moreButton.html() === "+ Selengkapnya") {
            $('span[name="point[' + id + ']"]').prop("hidden", true);
            $('span[name="more_text[' + id + ']"]').prop("hidden", false);
            moreButton.html("- Kurangi");
        } else {
            $('span[name="point[' + id + ']"]').prop("hidden", false);
            $('span[name="more_text[' + id + ']"]').prop("hidden", true);
            moreButton.html("+ Selengkapnya");
        }
    }
</script>
@endsection