@extends('home.layouts.template')

@section('contents')
<div class="mosh-breadcumb-area" style="background-image: url(<?= asset('themes/mosh/img/core-img/breadcumb.png') ?>);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="bradcumbContent">
                    <h2 class="text-left">Semua Jenis Kain Ulos</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mosh-projects-menu pt-3 m-0">
    <div class="text-center portfolio-menu m-0">
        <p class="active" style="font-size: 30px;" data-filter="*">Semua</p>
        @forelse ($usabilities as $usability)
        <p style="font-size: 30px;" data-filter=".{{ strtolower(str_replace(' ', '-', $usability->name)) }}">{{ $usability->name }}</p>
        @empty
        @endforelse
    </div>
</div>

<section class="mosh-aboutUs-area section_padding_100_0">
    <div class="container">
        <div class="mosh-portfolio pt-2 pb-5">
            @forelse ($items as $item)
            <div class="single_gallery_item
            @forelse ($item->usabilities as $usability)
            {{ ' ' . strtolower(str_replace(' ', '-', $usability->name)) }}
            @empty
            @endforelse
            ">
                <img src="{{ url($item->images->first()->file) }}" style="width:100%; height: 250px;" alt="">
                <div class="gallery-hover-overlay d-flex align-items-center justify-content-center">
                    <div class="port-hover-text text-center text-white">
                        <a href="{{ route('home.items.show', $item->id) }}">
                            <h4>{{ $item->name }}</h4>
                        </a>

                        {!! $item->usabilities && $item->usabilities->count() ? join(' dan ', array_filter(array_merge(array(join(', ', array_slice($item->usabilities->pluck('name')->toArray(), 0, -1))), array_slice($item->usabilities->pluck('name')->toArray(), -1)), 'strlen')) : '' !!}
                    </div>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</section>
@endsection