@extends('home.layouts.template')

@section('css')
<style>
    p {
        font-size: 18.72pt;
        color: black;
    }
</style>
@endsection

@section('contents')
<section class="welcome_area clearfix mb-5" id="home" style="background-image: url(<?= asset('assets/images/home-background.jpg') ?>); height: 450px">
    <div class="d-flex hero-slides owl-carousel">
        <div class="single-hero-slide d-flex justify-content-center">
            <div class="hero-slide-content text-center">
                <h2 class="text-center"><br><br>Kain Ulos Batak Toba</h2>
                <!-- <img src="{{ asset('themes/mosh/img/bg-img/device.png')}}" class="image-responsive" alt="logo"></a> -->
            </div>
        </div>
    </div>
</section>

@if ($items->count())
<section class="mosh-service-area clearfix pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="mosh-service-slides owl-carousel">
                    @foreach($items as $item)
                    @if ($item->images->count())
                    <div class="single-service-area text-center">
                        <img src="{{ url($item->images->first()->file) }}" class="image-responsive" alt="logo" style="height: 150px; width: 150px;"></a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<section class="mosh-aboutUs-area section_padding_100_0">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="mosh-about-us-content">
                    <div class="section-heading mb-4">
                        <h2>Apa itu Kain Ulos?</h2>
                    </div>
                    <p class="text-black">{!! $dashboard !!}</p>
                </div>
            </div>

            <!-- <div class="col-12 col-md-6">
                <div class="mosh-about-us-thumb wow fadeInUp" data-wow-delay="0.5s">
                    <img src="{{ asset('themes/mosh/img/bg-img/headpiece.png') }}" alt="">
                </div>
            </div> -->
        </div>
    </div>
</section>
@endsection