@extends('home.layouts.template')

@section('contents')
<div class="mosh-breadcumb-search" style="background-image: url(<?= url('themes/mosh/img/core-img/breadcumb.png') ?>)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 pt-5">
                <div class="bradcumbContent">
                    <h2>Hasil Penelusuran</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mosh-aboutUs-area section_padding_100_0" style="background-color: #e2e6f0;">
    <div class="container section_padding_50 pt-4 pb-4">
        <div class="row pb-5">
            <div class="col-12">
                <div class="subscribe-newsletter-content wow fadeIn" data-wow-delay="0.5s">
                    <form action="{{ route('home.searches.index') }}" method="POST" class="d-block aling-items-end">
                        @csrf
                        <input type="text" name="keywords" class="col-12" placeholder="Apa nama kain ulos yang ingin Anda cari nih..." value="{{ request()->keywords }}" style="font-size: 18pt; color: black">
                        <button type="submit" class="col-4">Pencarian</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @forelse ($items as $item)
    <div class="container section_padding_50 pt-4 pb-4">
        <div class="row align-items-center">
            <div class="col-12 col-md-3">
                <div class="mosh-team-slides owl-carousel">
                    @forelse ($item->images as $image)
                    <div class="single-team-slide text-center">
                        <img src="{{ url($image->file) }}" style="width:100%; height: 200px" alt="" />
                    </div>
                    @empty
                    @endforelse
                </div>
            </div>

            <div class="col-12 col-md-9">
                <div class="mosh-about-us-content">
                    <div class="section-heading1 mb-3">
                        <a href="{{ route('home.items.show', $item->id) }}">
                            <h2 class="text-justify">{{ $item->name }}</h2>
                        </a>
                    </div>
                    <p class="h5 text-justify">
                        @php
                        $string = strip_tags($item->description);
                        if (strlen($string) > 250) {
                        $stringCut = substr($string, 0, 250);
                        $endPoint = strrpos($stringCut, ' ');

                        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                        $string .= '...';
                        }
                        echo $string;
                        @endphp
                    </p>
                </div>
            </div>
        </div>
    </div>
    @empty
    <div class="container section_padding_50 pt-4 pb-4">
        <div class="row">
            <div class="col-12">
                <div class="mosh-about-us-content pt-5">
                    <div class="section-heading1 text-center mb-3">
                        <h4 class="font-italic text-muted">Wah, sepertinya {{ request()->keywords }} tidak ada pada daftar kain ulos.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforelse
</section>
@endsection