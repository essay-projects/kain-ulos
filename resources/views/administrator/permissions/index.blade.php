@extends('administrator.layouts.template')

@section('css')
<link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<style>
    .dataTables_length,
    .dataTables_filter,
    .dataTables_info,
    .dataTables_paginate {
        margin-left: 1.1rem !important;
        margin-right: 1.1rem !important;
    }

    .dataTables_length,
    .dataTables_info {
        text-align: left;
    }
</style>
@endsection

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>{{ $errors->first() }}</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="float-left m-0 mt-1">Permissions</h6>
                    <div class="float-right">
                        <button type="button" class="btn btn-sm btn-success" onclick="handleCreate()">
                            <i class="material-icons">add</i>
                            Create
                        </button>
                    </div>
                </div>

                <div class="card-body p-0 pt-3 pb-2 table-responsive">
                    <table id="table" class="table table-hover w-100">
                        <thead class="bg-light">
                            <tr class="text-center">
                                <th scope="col" class="border-0" style="width: 50px;">#</th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0" style="width: 180px; min-width: 180px;">Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="option" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="option-form" method="POST">
            @csrf
            <input type="hidden" id="option-method" name="_method" value="POST">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="option-title">Option</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="outline:none;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="option-name" class="col-form-label">Name</label>
                                <input type="text" id="option-name" name="name" class="form-control" placeholder="Name" value="" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="option-save" class="btn btn-primary">
                        <i class="fas fa-save"></i>
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
    const handleDestroy = id => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(willDelete => {
                if (willDelete) {
                    $("#option-form").attr("action", `{{ route('administrator.permissions.index') }}/${id}`);
                    $("#option-method").val("DELETE");
                    $("#option-form").submit();
                }
            });
    }

    const handleCreate = () => {
        $("#option-form").attr("action", `{{ route('administrator.permissions.store') }}`);
        $("#option-method").val('POST');
        $("#option-title").html("Create Permission");

        $("#option-name").val("");

        $("#option").modal("show");
    }

    $(document).on("click", ".edit", function() {
        const tr = $(this).parent().parent().parent().parent();
        const
            id = tr.data("id"),
            name = tr.data("name");

        $("#option-form").attr("action", `{{ route('administrator.permissions.index') }}/${id}`);
        $("#option-method").val('PUT');
        $("#option-title").html("Edit Permission");

        $("#option-name").val(name);

        $("#option").modal("show");
    });

    $("#option-form").submit(function(e) {
        e.preventDefault();
    }).validate({
        submitHandler: function(form) {
            $("#option-name").prop("readonly", true);

            $("#option-save").prop("disabled", true);
            $("#option-save").html("Loading...");

            form.submit();
        }
    });

    $("#table").DataTable({
        responsive: true,
        language: {
            paginate: {
                previous: "‹",
                next: "›"
            }
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ request()->url() }}",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            }
        },
        columns: [{
            data: "DT_RowIndex",
            orderable: false,
            searchable: false,
            bSortable: false
        }, {
            data: "name"
        }, {
            data: "column_actions",
            orderable: false,
            searchable: false
        }],
        order: [
            [1, "asc"]
        ],
        createdRow: function(row, data, indice) {
            $(row).data("id", data.id);
            $(row).data("name", data.name);

            $('td:eq(0)', row).addClass("align-middle text-center");
            $('td:eq(1)', row).addClass("align-middle");
            $('td:eq(2)', row).addClass("align-middle");
        }
    });
</script>
@endsection