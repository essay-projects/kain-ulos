<nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
    <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
        <div class="input-group input-group-seamless ml-3">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <h3 class="mt-3 font-weight-bold">KAIN ULOS</h3>
                </div>
            </div>
        </div>
    </form>

    <div class="w-100 d-none d-md-flex d-lg-flex"></div>

    <ul class="navbar-nav border-left flex-row ">
        {{-- <li class="nav-item border-right dropdown notifications">
            <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">&#xE7F4;</i>
                    <span class="badge badge-pill badge-danger">2</span>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">
                    <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                            <i class="material-icons">&#xE6E1;</i>
                        </div>
                    </div>
                    <div class="notification__content">
                        <span class="notification__category">Analytics</span>
                        <p>Your website’s active users count increased by
                            <span class="text-success text-semibold">28%</span> in the last week.
                            Great job!</p>
                    </div>
                </a>
                <a class="dropdown-item" href="#">
                    <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                            <i class="material-icons">&#xE8D1;</i>
                        </div>
                    </div>
                    <div class="notification__content">
                        <span class="notification__category">Sales</span>
                        <p>Last week your store’s sales count decreased by
                            <span class="text-danger text-semibold">5.52%</span>. It could have been
                            worse!</p>
                    </div>
                </a>
                <a class="dropdown-item notification__all text-center" href="#"> View all
                    Notifications </a>
            </div>
        </li> --}}

        <li class="nav-item border-right">
            <a class="nav-link nav-link-icon text-center" href="{{ route($routePrefix . '.settings.dashboards.index') }}" role="button">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">home</i>
                    <span class="badge badge-pill badge-{{ request()->routeIs($routePrefix . '.settings.dashboards.*') ? 'primary' : 'secondary' }}">Beranda</span>
                </div>
            </a>
        </li>

        <li class="nav-item border-right">
            <a class="nav-link nav-link-icon text-center" href="{{ route($routePrefix . '.settings.histories.index') }}" role="button">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">bookmark_added</i>
                    <span class="badge badge-pill badge-{{ request()->routeIs($routePrefix . '.settings.histories.*') ? 'primary' : 'secondary' }}">Sejarah</span>
                </div>
            </a>
        </li>

        <li class="nav-item border-right">
            <a class="nav-link nav-link-icon text-center" href="{{ route($routePrefix . '.usabilities.index') }}" role="button">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">group_work</i>
                    <span class="badge badge-pill badge-{{ request()->routeIs($routePrefix . '.usabilities.*') ? 'primary' : 'secondary' }}">Tipe</span>
                </div>
            </a>
        </li>

        <li class="nav-item border-right">
            <a class="nav-link nav-link-icon text-center" href="{{ route($routePrefix . '.items.index') }}" role="button">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">list_alt</i>
                    <span class="badge badge-pill badge-{{ request()->routeIs($routePrefix . '.items.*') ? 'primary' : 'secondary' }}">Jenis</span>
                </div>
            </a>
        </li>

        <li class="nav-item border-right">
            <a class="nav-link nav-link-icon text-center" href="{{ route($routePrefix . '.users.index') }}" role="button">
                <div class="nav-link-icon__wrapper">
                    <i class="material-icons">people_alt</i>
                    <span class="badge badge-pill badge-{{ request()->routeIs($routePrefix . '.users.*') ? 'primary' : 'secondary' }}">Pengguna</span>
                </div>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle mr-2" src="{{ asset(auth()->user()->person->avatar ? auth()->user()->person->avatar : 'assets/administrator/images/avatars/default.png') }}" alt="User Avatar">
                <span class="d-none d-md-inline-block mr-2">{{ auth()->user()->person->full_name }}</span>
            </a>

            <div class="dropdown-menu dropdown-menu-small">
                <a class="dropdown-item" href="{{ route($routePrefix . '.profile.index') }}">
                    <i class="material-icons">&#xE7FD;</i>
                    Profile
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout').submit();">
                    <i class="material-icons text-danger">&#xE879;</i>
                    {{ __('Logout') }}
                </a>
                <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>

    <!-- <nav class="nav">
        <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
            <i class="material-icons">&#xE5D2;</i>
        </a>
    </nav> -->
</nav>