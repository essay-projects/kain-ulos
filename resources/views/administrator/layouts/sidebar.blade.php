<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="{{ route($routePrefix . '.dashboards.index') }}" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <span class="d-md-inline ml-1" style="color: blue;">Kain Ulos</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>

    <div class="nav-wrapper">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{
                    request()->routeIs($routePrefix . '.index')
                    || request()->routeIs($routePrefix . '.dashboards*')
                    ? 'active' : ''
                }}" href="{{ route($routePrefix . '.dashboards.index') }}">
                    <i class="material-icons" style="width: 0px;">dashboards</i>
                    <span>Dasboard</span>
                </a>
            </li>

            @if ($authUser->hasRole($settingRole["administrator"]["superadmin"]))
            <li class="nav-item dropdown {{
                    request()->routeIs($routePrefix . '.permissions.*')
                    || request()->routeIs($routePrefix . '.roles.*')
                    ? 'show' : ''
                }}">
                <a class="nav-link dropdown-toggle {{
                    request()->routeIs('administrator.permissions.*')
                    || request()->routeIs($routePrefix . '.roles.*')
                    ? 'active' : ''
                }}" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small {{
                    request()->routeIs($routePrefix . '.permissions.*')
                    || request()->routeIs($routePrefix . '.roles.*')
                    ? 'show' : ''
                }}">
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.permissions.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.permissions.index') }}">
                        <span>Permissions</span>
                    </a>
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.roles.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.roles.index') }}">
                        <span>Roles</span>
                    </a>
                </div>
            </li>
            @endif

            @if ($authUser->hasRole($settingRole["administrator"]))
            <li class="nav-item dropdown {{
                    request()->routeIs($routePrefix . '.settings.*')
                    ? 'show' : ''
                }}">
                <a class="nav-link dropdown-toggle {{
                    request()->routeIs($routePrefix . '.settings.*')
                    ? 'active' : ''
                }}" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">settings</i>
                    <span>Pengaturan</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small {{
                    request()->routeIs($routePrefix . '.settings.*')
                    ? 'show' : ''
                }}">
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.settings.dashboards.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.settings.dashboards.index') }}">
                        <span>Dashboard</span>
                    </a>
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.settings.histories.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.settings.histories.index') }}">
                        <span>Sejarah</span>
                    </a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->routeIs($routePrefix . '.usabilities.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.usabilities.index') }}">
                    <i class="material-icons">group_work</i>
                    <span>Tipe Kegunaan</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->routeIs($routePrefix . '.items.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.items.index') }}">
                    <i class="material-icons">list_alt</i>
                    <span>Item Kain</span>
                </a>
            </li>

            <li class="nav-item dropdown {{
                    request()->routeIs($routePrefix . '.users.*')
                    ? 'show' : ''
                }}">
                <a class="nav-link dropdown-toggle {{
                    request()->routeIs($routePrefix . '.users.*')
                    ? 'active' : ''
                }}" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">people_alt</i>
                    <span>Pengguna</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small {{
                    request()->routeIs($routePrefix . '.users.*')
                    ? 'show' : ''
                }}">
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.users.*')  && !request()->routeIs($routePrefix . '.users.deleted.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.users.index') }}">
                        <span>Semua</span>
                    </a>
                    <a class="dropdown-item {{ request()->routeIs($routePrefix . '.users.deleted.*') ? 'active' : '' }}" href="{{ route($routePrefix . '.users.deleted.index') }}">
                        <span>Cadangan</span>
                    </a>
                </div>
            </li>
            @endif
        </ul>
    </div>
</aside>