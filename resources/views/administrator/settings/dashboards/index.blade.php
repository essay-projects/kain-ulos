@extends('administrator.layouts.template')

@section('content')
@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Beranda</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <form method="POST" action="{{ route('administrator.settings.dashboards.store') }}">
                        @csrf

                        <li class="list-group-item p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Deskripsi</strong>
                                    <div class="form-group">
                                        <textarea id="description" name="description" class="tiny-mce form-control @error('description') is-invalid @enderror" placeholder="Description...">{{ old('description', $dashboard->value) }}</textarea>

                                        @error("description")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                            <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                <i class="material-icons">save</i>
                                Simpan
                            </button>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: ".tiny-mce",
        theme: "modern",
        menubar: false,
        height: 300,
        setup: function(editor) {
            editor.on("change", function() {
                tinymce.triggerSave();
            });
        },
        browser_spellcheck: true
    });
</script>
@endsection