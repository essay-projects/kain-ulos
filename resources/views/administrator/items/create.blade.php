@extends('administrator.layouts.template')

@section('content')
@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Tambah Jenis Ulos</h6>
                </div>
                <ul class="list-group list-group-flush">
                    {{
                        Form::open([
                            "method" => "POST",
                            "route" => ["administrator.items.store"],
                            "files" => true
                        ])
                    }}

                    <li class="list-group-item p-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2" for="name">Nama</strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name') }}" required>

                                    @error("name")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Tipe Ulos</strong>
                                <div class="form-group">
                                    <div class="row">
                                        @foreach ($usabilities as $usabilityId => $usabilityName)
                                        <div class="col-3">
                                            <div class="custom-control custom-checkbox mb-1">
                                                <input type="checkbox" id="usability[{{ $usabilityId }}]" name="usability[]" class="custom-control-input" value="{{ $usabilityId }}" {{ old('usability') && in_array($usabilityId, old('usability')) ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="usability[{{ $usabilityId }}]">{{ $usabilityName }}</label>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    @if ($errors->has('usability'))
                                    <input type="hidden" class="form-control is-invalid">
                                    <div class="invalid-feedback">Something went wrong!</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2" for="description">Deskripsi</strong>
                                <div class="form-group">
                                    <textarea id="description" name="description" class="tiny-mce form-control @error('description') is-invalid @enderror" placeholder="Description...">{{ old('description') }}</textarea>

                                    @error("description")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Gambar</strong>
                                <div class="form-group">
                                    <div data-type="image">
                                        <div class="input-group mt-2" data-id="0">
                                            <div class="input-group-prepend">
                                            </div>
                                            <input type="file" name="image[0][file]" class="form-control @if($errors->has('image.*')) is-invalid @endif" required>
                                            <div class="input-group-append">
                                                <button type="button" class="list-destroy-button btn btn-sm btn-danger">
                                                    <i class="material-icons">delete</i>
                                                    Hapus
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-2 text-center">
                                        <button type="button" class="list-add-button btn btn-sm btn-success">
                                            <i class="material-icons">add</i>
                                            Tambah Gambar
                                        </button>
                                    </div>

                                    @if ($errors->has('image.*'))
                                    <input type="hidden" class="form-control is-invalid">
                                    <div class="invalid-feedback">Something went wrong.</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                        <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                            <i class="material-icons">save</i>
                            Simpan
                        </button>
                        <a class="btn btn-sm btn-secondary text-white mt-1 mb-1" href="{{ route('administrator.items.index') }}" style="width: 80px;">
                            <i class="material-icons">reply</i>
                            Kembali
                        </a>
                    </li>
                    {{ Form::close() }}
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: ".tiny-mce",
        theme: "modern",
        menubar: false,
        height: 200,
        setup: function(editor) {
            editor.on("change", function() {
                tinymce.triggerSave();
            });
        },
        browser_spellcheck: true
    });

    $(".list-add-button").on("click", function() {
        const list = $(this).parent().prev();
        const listType = list.data("type");
        const lastList = list.children().last();
        const currentListId = parseInt(lastList.data('id')) + 1;

        list.append(`
            <div class="input-group mt-2" data-id="${currentListId}">
                <div class="input-group-prepend">
                </div>
                <input type="file" name="${listType}[${currentListId}][file]" class="form-control" required>
                <div class="input-group-append">
                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">
                        <i class="material-icons">delete</i>
                        Hapus
                    </button>
                </div>
            </div>
        `);
    });

    $(document).on("click", ".list-destroy-button", function() {
        $(this).parent().parent().remove();
    });
</script>
@endsection