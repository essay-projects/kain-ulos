@extends('administrator.layouts.template')

@section('css')
<link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<style>
    .dataTables_length,
    .dataTables_filter,
    .dataTables_info,
    .dataTables_paginate {
        margin-left: 1.1rem !important;
        margin-right: 1.1rem !important;
    }

    .dataTables_length,
    .dataTables_info {
        text-align: left;
    }
</style>
@endsection

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="float-left m-0 mt-1">Pengguna</h6>
                    <div class="float-right">
                        <a href="{{ route('administrator.users.create') }}" class="btn btn-sm btn-success">
                            <i class="material-icons">add</i>
                            Tambah
                        </a>
                    </div>
                </div>

                <div class="card-body p-0 pt-3 pb-2 table-responsive">
                    <table id="table" class="table table-hover w-100">
                        <thead class="bg-light">
                            <tr class="text-center">
                                <th scope="col" class="border-0" style="width: 50px;">#</th>
                                <th scope="col" class="border-0">Nama</th>
                                <th scope="col" class="border-0">Peran</th>
                                <th scope="col" class="border-0" style="width: 85px;">Status</th>
                                <th scope="col" class="border-0" style="width: 180px; min-width: 180px;">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div hidden>
    <form id="option-form" method="POST">
        @csrf
        <input type="hidden" id="option-method" name="_method" value="POST">
    </form>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

<script>
    const handleDestroy = id => {
        swal({
                title: "Apakah anda yakin?",
                text: "Setelah dihapus, kamu tidak dapat mengembalikan item ini!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(willDelete => {
                if (willDelete) {
                    $("#option-form").attr("action", `{{ route('administrator.users.index') }}/${id}`);
                    $("#option-method").val("DELETE");
                    $("#option-form").submit();
                }
            });
    }

    $(document).on("change", ".toggle-status", function() {
        const csrfToken = $('meta[name="csrf-token"]').attr("content");
        const status = $(this).prop("checked") === true ? 1 : 0;
        const personId = $(this).parent().parent().parent().data("id");

        $.ajax({
            type: "POST",
            url: `<?= route('administrator.users.index') ?>/${personId}/status`,
            dataType: "json",
            data: {
                "_method": "PUT",
                "_token": csrfToken,
                "status": status
            },
            success: function(response) {
                console.log(response)
            }
        });
    });

    $("#table").DataTable({
        responsive: true,
        language: {
            paginate: {
                previous: "‹",
                next: "›"
            }
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ request()->url() }}",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            }
        },
        columns: [{
            data: "DT_RowIndex",
            orderable: false,
            searchable: false,
            bSortable: false
        }, {
            data: "column_name"
        }, {
            data: "column_roles"
        }, {
            data: "column_is_active"
        }, {
            data: "column_actions",
            orderable: false,
            searchable: false
        }],
        order: [
            [1, "asc"]
        ],
        createdRow: function(row, data, indice) {
            $(row).data("id", data.id);

            $('td:eq(0)', row).addClass("align-middle text-center");
            $('td:eq(1)', row).addClass("align-middle d-flex");
            $('td:eq(2)', row).addClass("align-middle text-center");
            $('td:eq(3)', row).addClass("align-middle");
            $('td:eq(4)', row).addClass("align-middle");
        }
    });
</script>
@endsection