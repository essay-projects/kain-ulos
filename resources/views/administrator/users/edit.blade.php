@extends('administrator.layouts.template')

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Pengguna</h6>
                </div>
                <ul class="list-group list-group-flush">
                    {{
                        Form::open([
                            "method" => "PUT",
                            "route" => ["administrator.users.update", $person->id],
                        ])
                    }}
                    <li class="list-group-item p-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Nama <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name', $person->full_name) }}" required>

                                    @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Email <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', $person->user->email) }}" required>

                                    @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        @if ($authUser->hasRole($settingRole["administrator"]))
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Peran <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group container-fluid row p-0 m-0">
                                    @foreach ($roles as $itemId => $item)
                                    <div class="col-md-3 col-sm-4 custom-control custom-checkbox mb-1">
                                        <input type="checkbox" id="role[{{ $itemId }}]" name="role[]" class="custom-control-input" value="{{ $itemId }}" {{ $person->user->hasRole($item) ? "checked" : "" }}>
                                        <label class="custom-control-label" for="role[{{ $itemId }}]">{{ $item }}</label>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="form-group m-0">
                                    <input class="form-control @error('role') is-invalid @enderror" hidden>
                                    @error("role")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @endif
                    </li>

                    <li class="list-group-item pt-3 pb-3 pl-4 pr-4">
                        <div class="clearfix flex-row">
                            <div class="float-left">
                                <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                    <i class="material-icons">save</i>
                                    Simpan
                                </button>

                                <a class="btn btn-sm btn-secondary text-white mt-1 mb-1 mr-2" href="{{ route("administrator.users.index") }}" style="width: 80px;">
                                    <i class="material-icons">reply</i>
                                    Kembali
                                </a>
                            </div>

                            <div class="float-right">
                                <button class="btn btn-sm btn-danger mt-1 mb-1" type="button" onclick="handleDelete()" style="width: 80px;">
                                    <i class="material-icons">delete</i>
                                    Hapus
                                </button>
                            </div>
                        </div>
                    </li>
                    {{ Form::close() }}
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Perbarui Foto User</h6>
                </div>
                <ul class="list-group list-group-flush">
                    {{
                        Form::model($person, [
                            "method" => "PUT",
                            "route" => ["administrator.users.update-avatar", $person->id],
                            "files" => true
                        ])
                    }}
                    <li class="list-group-item p-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-header text-center p-0 pb-3">
                                    <div class="mx-auto">
                                        <img class="rounded-circle" src="{{ url($person->avatar ?? '/assets/images/avatar.png') }}" alt="User Avatar" width="100">
                                    </div>
                                </div>

                                <div class="form-group m-0">
                                    <input type="file" name="avatar" class="form-control col-sm-12 @error('avatar') is-invalid @enderror">

                                    @error('avatar')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                        <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1" style="width: 80px;">
                            <i class="material-icons">save</i>
                            Simpan
                        </button>
                    </li>
                    {{ Form::close() }}
                </ul>
            </div>
        </div>

        <div class="col-lg-8">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Perbarui Password User</h6>
                </div>
                <ul class="list-group list-group-flush">
                    {{
                        Form::model($person, [
                            "method" => "PUT",
                            "route" => ["administrator.users.update-password", $person->id]
                        ])
                    }}
                    <li class="list-group-item p-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Current Password <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group">
                                    <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Current Password" autocomplete="new-password" required>

                                    @error('current_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <strong class="text-muted d-block mb-2">New Password <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group">
                                    <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="New Password" autocomplete="new-password" required>

                                    @error('new_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <strong class="text-muted d-block mb-2">New Confirm Password <small class="text-danger font-weight-bold">*</small></strong>
                                <div class="form-group">
                                    <input type="password" name="confirm_new_password" class="form-control" placeholder="Confirm" autocomplete="new-password" required>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                        <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1" style="width: 80px;">
                            <i class="material-icons">save</i>
                            Simpan
                        </button>
                    </li>
                    {{ Form::close() }}
                </ul>
            </div>
        </div>
    </div>
</div>

<div hidden>
    <form id="option-form" method="POST">
        @csrf
        <input type="hidden" id="option-method" name="_method" value="POST">
    </form>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDelete = () => {
        swal({
                title: "Apakah anda yakin?",
                text: "Setelah dihapus, kamu tidak dapat mengembalikan item ini!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $("#option-form").attr("action", `{{ route('administrator.users.destroy', $person->id) }}`);
                    $("#option-method").val("DELETE");
                    $("#option-form").submit();
                }
            });
    }
</script>
@endsection