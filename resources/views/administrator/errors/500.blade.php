@extends('administrator.layouts.template')

@section('content')
<div class="error" style="margin-top: -1.73rem; margin-bottom: -2rem;">
    <div class="error__content">
        <h2>500</h2>
        <h3>Something went wrong!</h3>
        <p>There was a problem on our end. Please try again later.</p>
        <a class="btn btn-accent btn-pill" href="{{ url()->previous() }}">&larr; Go Back</a>
    </div>
</div>
@endsection