@extends('administrator.layouts.template')

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Overview</span>
            <h3 class="page-title">User Profile</h3>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col-lg-4">
            <div class="card card-small mb-4 pt-3">
                <div class="card-header border-bottom text-center">
                    <div class="mb-3 mx-auto">
                        <img class="rounded-circle" src="{{ url($authUser->person->avatar ? $authUser->person->avatar : 'assets/images/avatar.png') }}" alt="User Avatar" width="110"> </div>
                    <h4 class="mb-0">{{ $authUser->person->full_name }}</h4>
                    <span class="text-muted d-block mb-2">{{ $authUser->roles->first()->name }}</span>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-2 pl-4 pr-4">
                        <strong class="text-muted d-block mt-2 mb-2">{{ $authUser->email }}</strong>
                    </li>

                    <li class="list-group-item p-3 pb-2">
                        {{
                            Form::open([
                                "method" => "PUT",
                                "route" => [$routePrefix . ".profile.update-avatar"],
                                "files" => true
                            ])
                        }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-12 mb-0">
                                            <label>Update Avatar</label>
                                            <input type="file" name="avatar" class="form-control col-sm-12 @error('avatar') is-invalid @enderror">
                                        </div>
                                    </div>

                                    @error('avatar')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="material-icons">save</i>
                                    Update Avatar
                                </button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </li>
                </ul>

            </div>
        </div>

        <div class="col-lg-8">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Account Details</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col">
                                {{
                                    Form::open([
                                        "method" => "PUT",
                                        "route" => [$routePrefix . ".profile.update"]
                                    ])
                                }}
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', $authUser->person->full_name) }}" required>

                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', $authUser->email) }}" required>

                                        @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                @if ($authUser->hasRole($settingRole["administrator"]))
                                <div class="form-row">
                                    <div class="form-group col-sm-12">
                                        <label class="d-block mb-2">Role</label>
                                        <div class="form-group container-fluid row p-0 m-0">
                                            @foreach ($roles as $itemId => $item)
                                            <div class="col-md-3 col-sm-4 custom-control custom-checkbox mb-1">
                                                <input type="checkbox" id="role[{{ $itemId }}]" name="role[]" class="custom-control-input" value="{{ $itemId }}" {{ $authUser->hasRole($item) ? "checked" : "" }}>
                                                <label class="custom-control-label" for="role[{{ $itemId }}]">{{ $item }}</label>
                                            </div>
                                            @endforeach
                                        </div>

                                        <div class="form-group m-0">
                                            <input class="form-control @error('role') is-invalid @enderror" hidden>
                                            @error("role")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <button type="submit" class="float-left btn btn-primary">
                                    <i class="material-icons">save</i>
                                    Update
                                </button>
                                <button type="button" class="float-right btn btn-danger" onclick="handleDestroy()">
                                    <i class="material-icons">delete</i>
                                    Delete
                                </button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-small mb-4">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Update Password</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        {{
                                            Form::open([
                                                "method" => "PUT",
                                                "route" => [$routePrefix . ".profile.update-password"]
                                            ])
                                        }}
                                        <div class="form-row">
                                            <div class="col-sm-12">
                                                <label>Current Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Current Password" autocomplete="new-password" required>

                                                    @error('current_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <label>New Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="New Password" autocomplete="new-password" required>

                                                    @error('new_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>New Confirm Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_confirm_password" class="form-control" placeholder="Confirm" autocomplete="new-password" required>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons">save</i>
                                            Update Password
                                        </button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div hidden>
    <form id="option-form" method="POST">
        @csrf
        <input type="hidden" id="option-method" name="_method" value="POST">
    </form>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDestroy = () => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $("#option-form").attr("action", `{{ route($routePrefix . '.profile.destroy') }}`);
                    $("#option-method").val("DELETE");
                    $("#option-form").submit();
                }
            });
    }
</script>
@endsection