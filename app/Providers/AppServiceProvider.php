<?php

namespace App\Providers;

use App\Models\Usability;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $settings = config("settings");

            if (auth()->check()) {
                $authUser = auth()->user();
                $authPerson = $authUser->person;

                if ($authUser->hasRole($settings["role"]["administrator"])) {
                    $routePrefix = "administrator";
                } else {
                    $routePrefix = "home";
                }
            } else {
                $authUser = "";
                $authPerson = "";
                $routePrefix = "home";
            }

            $usabilities = Usability::all();

            $view->with([
                "settingRole" => $settings["role"],
                "settingDay" => $settings["day"],
                "settingMonth" => $settings["month"],
                "authUser" => $authUser,
                "authPerson" => $authPerson,
                "routePrefix" => $routePrefix,
                "varUsabilities" => $usabilities
            ]);
        });
    }
}
