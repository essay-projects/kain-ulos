<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        "name",
        "description"
    ];

    public function usabilities()
    {
        return $this->belongsToMany(Usability::class, "item_usabilities", "item_id", "usability_id");
    }

    public function images()
    {
        return $this->hasMany(ItemImage::class);
    }
}
