<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemUsability extends Model
{
    protected $fillable = [
        "item_id",
        "usability_id"
    ];
}
