<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usability extends Model
{
    protected $fillable = [
        "name"
    ];

    public function items()
    {
        return $this->belongsToMany(Item::class, "item_usabilities", "usability_id", "item_id");
    }
}
