<?php

namespace App\Http\Controllers\Administrator\Usabilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Usability;

class UsabilityController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $usabilities = Usability::orderBy("name")->get();
            return datatables()->of($usabilities)
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="edit btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </button>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Hapus
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.usabilities.index"
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:usabilities,name"
        ]);

        Usability::updateOrCreate(
            [
                "name" => $request->name
            ],
            []
        );

        return redirect()->back()->with("success", "Tipe ulos berhasil ditambahkan.");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        Usability::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name
            ]
        );

        return redirect()->back()->with("success", "Tipe ulos berhasil diperbarui.");
    }

    public function destroy($id)
    {
        Usability::findOrFail($id)->delete();
        return redirect()->back()->with("success", "Tipe ulos berhasil dihapus.");
    }
}
