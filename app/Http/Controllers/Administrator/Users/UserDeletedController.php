<?php

namespace App\Http\Controllers\Administrator\Users;

use App\Http\Controllers\Controller;

use App\Models\Person;

class UserDeletedController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");
        $this->settingMonth = config("settings.month");

        $this->middleware(function ($request, $next) {
            $this->authUser = auth()->user();
            return $next($request);
        });
    }

    public function index()
    {
        if (request()->ajax()) {
            if ($this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
                $people = Person::onlyTrashed()->whereHas("user")->orderBy("full_name")->get();
            } else {
                $people = Person::onlyTrashed()->orderBy("full_name")->whereHas("user.roles", function ($roles) {
                    $roles->where("name", "!=", $this->settingRole["administrator"]["superadmin"]);
                })->get();
            }

            return datatables()->of($people)
                ->addColumn("column_name", function ($data) {
                    return '
                    <img class="rounded-circle mr-2 float-left" src="' . asset($data->avatar ?? '/assets/images/avatar.png') . '" style="height: 2.7rem;" />
                    <div class="float-left">
                        ' . $data->full_name . '
                        <small class="d-flex mt-1">' . $data->user->username ?? $data->user->email  . '</small>
                    </div>
                    ';
                })
                ->addColumn("column_deleted_at", function ($data) {
                    return (int) date("d", strtotime($data->deleted_at)) . " " .
                        $this->settingMonth[date("m", strtotime($data->deleted_at))] . " " .
                        date("Y, H:i:s", strtotime($data->deleted_at));
                })
                ->addColumn("column_roles", function ($data) {
                    $roles = '';
                    if ($data->user->roles) {
                        foreach ($data->user->roles->sortBy('name') as $role) {
                            $roles .= '<small class="btn btn-sm btn-pill btn-warning p-1 pr-3 pl-3 m-1">' . $role->name . '</small>';
                        }
                    } else {
                        $roles = '<small class="font-italic m-1">Role Not Found</small>';
                    }

                    return $roles;
                })
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-info col-sm-12" onclick="handleRestore(' . $data->id . ')">
                                <i class="material-icons">restore</i>
                                Restore
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.users.deleted.index"
        );
    }

    public function restore($id)
    {
        $person = Person::onlyTrashed()->find($id);
        $person->user->update(["is_active" => 1]);
        $person->restore();

        return redirect()->route(
            "administrator.users.deleted.index"
        )->with("success", "User successfully restored.");
    }
}
