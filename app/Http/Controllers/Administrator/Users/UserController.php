<?php

namespace App\Http\Controllers\Administrator\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Models\Person;

class UserController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUser = auth()->user();
            return $next($request);
        });
    }

    public function index()
    {
        if (request()->ajax()) {
            if ($this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
                $people = Person::orderBy("full_name")->whereHas("user")->get();
            } else {
                $people = Person::orderBy("full_name")->whereHas("user.roles", function ($roles) {
                    $roles->where("name", "!=", $this->settingRole["administrator"]["superadmin"]);
                })->get();
            }

            return datatables()->of($people)
                ->addColumn("column_name", function ($data) {
                    return '
                    <img class="rounded-circle mr-2 float-left" src="' . url($data->avatar ?? '/assets/images/avatar.png') . '" style="height: 2.7rem;" />
                    <div class="float-left">
                        ' . $data->full_name . '
                        <small class="d-flex mt-1">' . $data->user->username ?? $data->user->email  . '</small>
                    </div>
                    ';
                })
                ->addColumn("column_roles", function ($data) {
                    $roles = '';
                    if ($data->user->roles->count()) {
                        foreach ($data->user->roles->sortBy('name') as $role) {
                            $roles .= '<small class="btn btn-sm btn-pill btn-warning p-1 pr-3 pl-3 m-1">' . $role->name . '</small>';
                        }
                    } else {
                        $roles = '<small class="font-italic m-1">Role Not Found</small>';
                    }

                    return $roles;
                })
                ->addColumn("column_is_active", function ($data) {
                    return '
                    <div class="custom-control custom-toggle custom-toggle">
                        <input type="checkbox" id="set-is-active[' . $data->id . ']" class="custom-control-input toggle-status" ' . ($data->user->is_active ? "checked" : "") . '>
                        <label class="custom-control-label" for="set-is-active[' . $data->id . ']"></label>
                    </div>
                    ';
                })
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <a href="' . route('administrator.users.edit', $data->id) . '"  class="btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </a>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Hapus
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.users.index"
        );
    }

    public function create()
    {
        if ($this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
            $roles = Role::orderBy("name")->pluck("name", "id")->all();
        } else {
            $roles = Role::orderBy("name")->where("name", "!=", $this->settingRole["administrator"]["superadmin"])->pluck("name", "id")->all();
        }

        return view(
            "administrator.users.create",
            compact("roles")
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users",
            // "username" => "required|string|max:255|unique:users",
            "password" => "required|string|min:6|confirmed"
        ]);

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            $this->validate($request, [
                "role" => "required"
            ]);
        }

        $person = Person::updateOrCreate(
            [
                "full_name" => $request->input("name")
            ]
        );

        $user = User::updateOrCreate(
            [
                "person_id" => $person->id
            ],
            [
                "username" => $request->username,
                "password" => Hash::make($request->password),
                "email" => $request->email ?? NULL
            ]
        );

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            $user->assignRole($request->role);
        }

        return redirect()->route(
            "administrator.users.index"
        )->with("success", "User berhasil ditambahkan.");
    }

    public function edit($id)
    {
        $person = Person::find($id);

        if ($this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
            $roles = Role::orderBy("name")->pluck("name", "id")->all();
        } else {
            $roles = Role::orderBy("name")->where("name", "!=", $this->settingRole["administrator"]["superadmin"])->pluck("name", "id")->all();
        }

        return view(
            "administrator.users.edit",
            compact("person", "roles")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users,email," . $id,
            // "username" => "required|string|max:255|unique:users,username," . $id,
        ]);

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            $this->validate($request, [
                "role" => "required"
            ]);
        }

        $person = Person::find($id);
        $person->update(
            [
                "full_name" => $request->name
            ]
        );

        $user = User::updateOrCreate(
            [
                "person_id" => $person->id
            ],
            [
                "username" => $request->username ?? $person->user->username,
                "email" => $request->email
            ]
        );

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            DB::table("model_has_roles")->where("model_id", $user->id)->delete();
            $user->assignRole($request->role);
        }

        return redirect()->route(
            "administrator.users.edit",
            $id
        )->with("success", "User berhasil diperbarui.");
    }

    public function destroy($id)
    {
        if ($this->authUser->id === (int) $id) {
            Auth::logout();
        }

        $person = Person::find($id);
        $person->user->update(["is_active" => 0]);
        $person->delete();

        return redirect()->route(
            "administrator.users.index"
        )->with("success", "User berhasil dihapus.");
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            "current_password" => "required",
            "new_password" => "required|same:confirm_new_password",
        ]);

        $person = Person::find($id);
        if (Hash::check($request->current_password, $person->user->password)) {
            $person->user->update(["password" => Hash::make($request->new_password)]);
            return redirect()->back()->with("success", "Password berhasil diperbarui.");
        } else {
            return redirect()->back()->withErrors(["current_password" => "The attribute is match with old password."]);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $person = Person::find($id);
        $person->user->update(["is_active" => $request->status]);

        if ($this->authUser->id === $person->user->id && $request->status === "0") {
            Auth::logout();
        }

        return response()->json([
            "status" => true,
            "message" => "Status change successfully."
        ]);
    }

    public function updateAvatar(Request $request, $id)
    {
        $request->validate([
            "avatar"        => "required|mimes:jpg,jpeg,png"
        ]);

        if ($request->file("avatar")) {
            $person = Person::find($id);

            $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $request->file("avatar")->extension();
            $filePath = $request->file("avatar")->storeAs(
                "storage/images/avatars",
                $fileName,
                "public"
            );

            if ($filePath) {
                if (!preg_match("/^(\/)?seeds/i", $person->avatar)) {
                    Storage::disk("public")->delete($person->avatar);
                }

                Person::updateOrCreate(
                    [
                        "id"     => $person->id
                    ],
                    [
                        "avatar"    => $filePath
                    ]
                );

                return redirect()->route(
                    "administrator.users.edit",
                    $id
                )->with("success", "Avatar berhasil diperbarui.");
            } else {
                return redirect()->route(
                    "administrator.users.edit",
                    $id
                )->withErrors(["avatar" => "File upload failed."]);
            }
        } else {
            return redirect()->route(
                "administrator.users.edit",
                $id
            )->withErrors(["avatar" => "File not found."]);
        }
    }
}
