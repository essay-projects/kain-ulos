<?php

namespace App\Http\Controllers\Administrator\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware("permission:role-list|role-create|role-edit|role-delete", ["only" => ["index", "store"]]);
        $this->middleware("permission:role-create", ["only" => ["create", "store"]]);
        $this->middleware("permission:role-edit", ["only" => ["edit", "update"]]);
        $this->middleware("permission:role-delete", ["only" => ["destroy"]]);
    }

    public function index()
    {
        if (request()->ajax()) {
            $roles = Role::orderBy("name")->get();
            return datatables()->of($roles)
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <a href="' . route('administrator.roles.edit', $data->id) . '"  class="btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </a>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Destroy
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.roles.index"
        );
    }

    public function create()
    {
        $permissions = Permission::all()->sortBy("name");
        return view(
            "administrator.roles.create",
            compact("permissions")
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:roles,name",
        ]);

        $role = Role::create(
            [
                "name" => $request->name
            ],
            []
        );

        if ($request->permission) {
            $role->syncPermissions($request->permission);
        }

        return redirect()->route(
            "administrator.roles.index"
        )->with("success", "Role created successfully.");
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all()->sortBy("name");
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck("role_has_permissions.permission_id", "role_has_permissions.permission_id")
            ->all();

        return view(
            "administrator.roles.edit",
            compact("role", "permissions", "rolePermissions")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required",
        ]);

        $role = Role::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name
            ]
        );

        if ($request->permission) {
            $role->syncPermissions($request->permission);
        } else {
            DB::table("role_has_permissions")->where("role_id", $id)->delete();
        }

        return redirect()->route(
            "administrator.roles.edit",
            $role->id
        )->with("success", "Role updated successfully.");
    }

    public function destroy($id)
    {
        Role::findOrFail($id)->delete();
        return redirect()->route(
            "administrator.roles.index"
        )->with("success", "Role deleted successfully.");
    }
}
