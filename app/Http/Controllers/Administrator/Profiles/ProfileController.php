<?php

namespace App\Http\Controllers\Administrator\Profiles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Models\Person;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUser = auth()->user();
            return $next($request);
        });
    }

    public function index()
    {
        $roles = Role::orderBy("name")->pluck("name", "id")->all();
        if (!$this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
            $superadminRoleId = Role::where("name", $this->settingRole["administrator"]["superadmin"])->pluck("id")->first();
            unset($roles[$superadminRoleId]);
        }

        return view(
            "administrator.profile.index",
            compact("roles")
        );
    }

    public function update(Request $request)
    {
        $id = $this->authUser->id;
        $this->validate($request, [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users,email," . $id
            // "username" => "required|string|max:255|unique:users,username," . $id
        ]);

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            $this->validate($request, [
                "role" => "required"
            ]);
        }

        $person = $this->authUser->person;
        $person->update(
            [
                "full_name" => $request->name
            ]
        );

        $user = User::updateOrCreate(
            [
                "person_id" => $person->id
            ],
            [
                "username" => $request->username ?? $person->user->username,
                "email" => $request->email
            ]
        );

        if ($this->authUser->hasRole($this->settingRole["administrator"])) {
            DB::table("model_has_roles")->where("model_id", $user->id)->delete();
            $user->assignRole($request->role);
        }

        return redirect()->back()->with("success", "Success! Your profile has been updated!");
    }

    public function destroy()
    {
        $this->authUser->logout();
        $this->authUser->update(["is_active" => 0]);
        $this->authUser->person->delete();
        // $this->authUser->delete();

        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            "current_password" => ["required", new MatchOldPassword],
            "new_password" => "required|same:new_confirm_password",
        ]);

        $this->authUser->update(["password" => Hash::make($request->new_password)]);

        return redirect()->back()->with("success", "Success! Your password has been updated!");
    }

    public function updateAvatar(Request $request)
    {
        $request->validate([
            "avatar"        => "required|mimes:jpg,jpeg,png"
        ]);

        if ($request->file("avatar")) {
            $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $request->file("avatar")->extension();
            $filePath = $request->file("avatar")->storeAs(
                "storage/images/avatars",
                $fileName,
                "public"
            );

            $person = $this->authUser->person;
            if ($filePath) {
                if (!preg_match("/^(\/)?seeds/i", $person->avatar)) {
                    Storage::disk("public")->delete($person->avatar);
                }

                Person::updateOrCreate(
                    [
                        "id"     => $person->id
                    ],
                    [
                        "avatar"    => $filePath
                    ]
                );

                return redirect()->back()->with("success", "Success! Your avatar has been updated!");
            } else {
                return redirect()->back()->withErrors(["avatar" => "File upload failed."]);
            }
        } else {
            return redirect()->back()->withErrors(["avatar" => "File not found."]);
        }
    }
}
