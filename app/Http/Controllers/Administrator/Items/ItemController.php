<?php

namespace App\Http\Controllers\Administrator\Items;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemUsability;
use App\Models\Usability;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUser = auth()->user();
            return $next($request);
        });
    }

    public function index()
    {
        if (request()->ajax()) {
            $items = Item::orderBy("name")->get();
            return datatables()->of($items)
                ->addColumn("column_usabilities", function ($data) {
                    if ($data->usabilities && $data->usabilities->count()) {
                        $usabilities = '';
                        foreach ($data->usabilities->sortBy('name') as $usability) {
                            $usabilities .= '<small class="btn btn-sm btn-pill btn-warning p-1 pr-3 pl-3 m-1">' . $usability->name . '</small>';
                        }
                    } else {
                        $usabilities = '<small class="font-italic m-1">Usability Not Found</small>';
                    }

                    return $usabilities;
                })
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <a href="' . route('administrator.items.edit', $data->id) . '"  class="btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </a>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Hapus
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.items.index"
        );
    }

    public function create()
    {
        $usabilities = Usability::orderBy("name")->pluck("name", "id")->all();

        return view(
            "administrator.items.create",
            compact("usabilities")
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "description" => "required",
            "usability" => "required|array",
            "usability.*" => "required|exists:usabilities,id",
            'image' => 'required',
            'image.*.file' => 'image|mimes:jpg,jpeg,png'
        ]);

        $item = Item::updateOrCreate(
            [
                "name" => $request->name,
                "description" => $request->description
            ]
        );

        if (count($request->usability) > 0) {
            $usabilities = [];
            foreach ($request->usability as $usabilityId) {
                $usability = Usability::find($usabilityId);
                if ($usability) {
                    $usabilities[] = $usability->id;
                }
            }

            $item->usabilities()->sync($usabilities);
        }

        if (count($request->file("image")) > 0) {
            foreach ($request->file("image") as $image) {
                $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $image["file"]->extension();
                $filePath = $image["file"]->storeAs(
                    "storage/images/items",
                    $fileName,
                    "public"
                );

                if ($filePath) {
                    ItemImage::create([
                        "item_id" => $item->id,
                        "file" => $filePath
                    ]);
                }
            }
        }

        return redirect()->route(
            "administrator.items.index"
        )->with("success", "Jenis kain berhasil ditambahkan.");
    }

    public function edit($id)
    {
        $item = Item::findOrFail($id);
        $usabilities = Usability::orderBy("name")->pluck("name", "id")->all();

        return view(
            "administrator.items.edit",
            compact("item", "usabilities")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required",
            "description" => "required",
            "usability" => "required|array",
            "usability.*" => "required|exists:usabilities,id",
            'image.*.file' => 'nullable|image|mimes:jpg,jpeg,png'
        ]);

        $item = Item::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name,
                "description" => $request->description
            ]
        );

        if (count($request->usability) > 0) {
            $usabilities = [];
            foreach ($request->usability as $usabilityId) {
                $usability = Usability::find($usabilityId);
                if ($usability) {
                    $usabilities[] = $usability->id;
                }
            }

            $item->usabilities()->sync($usabilities);
        } else {
            $item->usabilities()->sync([]);
        }

        $toBeDeleted = $item->images()->pluck("id", "id")->toArray();
        $tempImages = @$_FILES["image"]["name"];
        if ($tempImages) {
            foreach ($tempImages as $imageId => $tempImage) {
                if ($request->file("image") && array_key_exists($imageId, $request->file("image"))) {
                    $newImage = $request->file("image")[$imageId];
                    $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $newImage["file"]->extension();
                    $filePath = $newImage["file"]->storeAs(
                        "storage/images/items",
                        $fileName,
                        "public"
                    );

                    if ($filePath) {
                        $image = ItemImage::find($imageId);
                        if ($image) {
                            if (!preg_match("/^(\/)?seeds/i", $image->file)) {
                                Storage::disk("public")->delete($image->file);
                            }
                        } else {
                            $imageId = NULL;
                        }

                        ItemImage::updateOrCreate(
                            [
                                "id" => $imageId
                            ],
                            [
                                "item_id" => $item->id,
                                "file" => $filePath
                            ]
                        );
                    }
                }

                unset($toBeDeleted[$imageId]);
            }
        }

        if ($toBeDeleted) {
            foreach ($toBeDeleted as $toBeDeletedId) {
                $image = ItemImage::find($toBeDeletedId);
                if ($image && !preg_match("/^(\/)?seeds/i", $image->file)) {
                    Storage::disk("public")->delete($image->file);
                    $image->delete();
                }
            }
        }

        return redirect()->back()->with("success", "Jenis kain berhasil diperbarui.");
    }

    public function destroy($id)
    {
        Item::findOrFail($id)->delete();
        return redirect()->back()->with("success", "Jenis kain berhasil dihapus.");
    }
}
