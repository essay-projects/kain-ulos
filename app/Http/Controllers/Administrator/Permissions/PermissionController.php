<?php

namespace App\Http\Controllers\Administrator\Permissions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    function __construct()
    {
        $this->middleware("permission:permission-list|permission-create|permission-edit|permission-delete", ["only" => ["index", "store"]]);
        $this->middleware("permission:permission-create", ["only" => ["create", "store"]]);
        $this->middleware("permission:permission-edit", ["only" => ["edit", "update"]]);
        $this->middleware("permission:permission-delete", ["only" => ["destroy"]]);
    }

    public function index()
    {
        if (request()->ajax()) {
            $permissions = Permission::orderBy("name")->get();
            return datatables()->of($permissions)
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="edit btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </button>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Destroy
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.permissions.index"
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:roles,name"
        ]);

        Permission::updateOrCreate(
            [
                "name" => $request->name,
                "guard_name" => "web"
            ],
            []
        );

        return redirect()->back()->with("success", "Permission created successfully.");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        Permission::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name
            ]
        );

        return redirect()->back()->with("success", "Permission updated successfully.");
    }

    public function destroy($id)
    {
        Permission::findOrFail($id)->delete();
        return redirect()->back()->with("success", "Permission deleted successfully.");
    }
}
