<?php

namespace App\Http\Controllers\Administrator\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Setting;

class DashboardController extends Controller
{
    public function index()
    {
        $dashboard = Setting::find("dashboard");

        return view(
            "administrator.settings.dashboards.index",
            compact("dashboard")
        );
    }

    public function store(Request $request)
    {
        Setting::find("dashboard")->update(
            [
                "value" => $request->description
            ]
        );

        return redirect()->back()->with("success", "Beranda berhasil diperbarui.");
    }
}
