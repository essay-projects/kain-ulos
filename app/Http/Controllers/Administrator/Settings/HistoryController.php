<?php

namespace App\Http\Controllers\Administrator\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Setting;

class HistoryController extends Controller
{
    public function index()
    {
        $history = Setting::find("history");

        return view(
            "administrator.settings.histories.index",
            compact("history")
        );
    }

    public function store(Request $request)
    {
        Setting::find("history")->update(
            [
                "value" => $request->description
            ]
        );

        return redirect()->back()->with("success", "Sejarah berhasil diperbarui.");
    }
}
