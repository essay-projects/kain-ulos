<?php

namespace App\Http\Controllers\Home\Dashboards;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Setting;

class DashboardController extends Controller
{
    public function index()
    {
        $dashboard = Setting::findOrFail("dashboard")->value;
        $items = Item::latest("created_at")->take(10)->get();

        return view(
            "home.dashboards.index",
            compact("dashboard", "items")
        );
    }
}
