<?php

namespace App\Http\Controllers\Home\Histories;

use App\Http\Controllers\Controller;
use App\Models\Setting;

class HistoryController extends Controller
{
    public function index()
    {
        $history = Setting::findOrFail("history")->value;

        return view(
            "home.histories.index",
            compact("history")
        );
    }
}
