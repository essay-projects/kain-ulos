<?php

namespace App\Http\Controllers\Home\Items;

use App\Http\Controllers\Controller;
use App\Models\Item;

class ItemController extends Controller
{
    public function show($id)
    {
        $item = Item::findOrFail($id);

        return view(
            "home.items.show",
            compact("item")
        );
    }
}
