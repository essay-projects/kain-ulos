<?php

namespace App\Http\Controllers\Home\Searches;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $items = Item::where("name", "like", "%" . $request->keywords . "%")->get();

        return view(
            "home.searches.index",
            compact("items")
        );
    }
}
