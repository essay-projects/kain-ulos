<?php

namespace App\Http\Controllers\Home\Usabilities;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Usability;

class UsabilityController extends Controller
{
    public function index()
    {
        $usabilities = Usability::all();
        $items = Item::all();

        return view(
            "home.usabilities.index",
            compact("usabilities", "items")
        );
    }

    public function show($id)
    {
        $usability = Usability::findOrFail($id);

        return view(
            "home.usabilities.show",
            compact("usability")
        );
    }
}
