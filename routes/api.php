<?php

use Illuminate\Support\Facades\Route;

Route::group([
    "namespace" => "Api",
    "as" => "api."
], function () {
    Route::get("/", function () {
        return response()->json([
            "meta" => [
                "code" => 200,
                "message" => "Made with ♡ by sintasTech",
                "error" => false
            ]
        ]);
    });

    Route::post("login", "AuthController@login");
    Route::post("register", "AuthController@register");

    Route::middleware(["jwt.verify"])->group(function () {
        Route::get("auth", "AuthController@getAuthenticatedUser");
    });
});
